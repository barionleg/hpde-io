<Spase xmlns="http://www.spase-group.org/data/schema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.spase-group.org/data/schema  https://www.spase-group.org/data/schema/spase-2_4_0.xsd">
 <Version>2.4.0</Version>
 <NumericalData>
  <ResourceID>spase://CSA/NumericalData/GO-ABOVE/VLF/L2/BarrierLake/PT1M</ResourceID>
  <ResourceHeader>
   <ResourceName>ABOVE Level-2 Amplitude and Phase of VLF transmitter
   signals at Barrier Lake (barr)</ResourceName>
   <ReleaseDate>2021-10-05T17:42:52</ReleaseDate>
   <Description>The Array for Broadband Observations of VLF/ELF Emissions (ABOVE) Level-2 CDF files of Amplitude, Phase and Bearing
   at 1 minute resolution for seven VLF transmitters as observed from each station of the ABOVE array.  The name of the ABOVE station
   is given in the filename and file metadata information. The VLF transmitters observed are:
   
   NDK (LaMoure, North Dakota, 46.37 N, 98.33 W, 25.2 kHz)

   NAA (Cutler, Maine,  44.64 N, 67.29 W, 24.0 kHz),
   
   NLK (Seattle, Washington, 48.20 N 121.92 W, 24.8 kHz)

   NWC (Exmouth, Western Australia, 21.82 S, 114.16 E, 19.8 kHz)
   
   NPM (Lualualei, Hawaii, 21.42 N, 158.15 W, 21.4 kHz)

   NRK (Keflavik, Iceland, 63.85 N, 22.45 W, 37.5 kHz)

   WWVB (Fort Collins, Colorado, 40.68 N, 105.05 W, 60 kHz)

   GO-ABOVE is a ground‐based array of VLF radio receivers located across Canada and run as a part of the Canadian Space Agency&#039;s Geospace Observatory (GO). The ABOVE instruments are situated in two north‐south chains in Alberta and Manitoba, through the region roughly from L = 3 through L = 8.  The ABOVE instruments monitor electromagnetic waves in a frequency range from 100 Hz to 75 kHz, covering chorus and hiss emissions as well as U.S. Navy transmitters near 25 kHz. The absolute timing accuracy is 10 ns, from GPS.    
   </Description>
   <Acknowledgement>Please include the following in any publications using this data: Infrastructure funding for ABOVE is provided by the Canada Foundation for Innovation and the province of Alberta. ABOVE data used in this work was collected with the support of the Canadian Space Agency Geospace Observatory (GO Canada) contribution initiative.</Acknowledgement>
   <Contact>
    <PersonID>spase://SMWG/Person/C.M.Cully</PersonID>
    <Role>PrincipalInvestigator</Role>
   </Contact>
   <Contact>
    <PersonID>spase://SMWG/Person/Leonard.N.Garcia</PersonID>
    <Role>MetadataContact</Role>
   </Contact>
  </ResourceHeader>
  <AccessInformation>
   <RepositoryID>spase://SMWG/Repository/UCalgary</RepositoryID>
   <AccessURL>
    <URL>https://data.phys.ucalgary.ca/sort_by_instrument/vlf/GO-Canada_ABOVE/CDF/</URL>
    <Description>Directory tree to access Level-1 and Level-2 CDF data.</Description>
   </AccessURL>
   <Format>CDF</Format>
  </AccessInformation>
  <InstrumentID>spase://SMWG/Instrument/BarrierLake/VLF</InstrumentID>
  <MeasurementType>Waves.Passive</MeasurementType>
  <TemporalDescription>
   <TimeSpan>
    <StartDate>2014-08-13T18:00:00</StartDate>
    <StopDate>2019-10-01T05:45:00</StopDate>
   </TimeSpan>
   <Cadence>PT1M</Cadence>
  </TemporalDescription>
  <SpectralRange>RadioFrequency</SpectralRange>
  <ObservedRegion>Earth.NearSurface.Ionosphere</ObservedRegion>
  <Parameter>
   <Name>Epoch</Name>
   <Support>
    <SupportQuantity>Temporal</SupportQuantity>
   </Support>
  </Parameter>
  <Parameter>
   <Name>Phase observed from ABOVE station to NDK</Name>
   <Description>Phase of VLF transmitter NDK
Phases for upper (space) and lower (mark) frequencies have been combined. Phase is calculated along direction of maximum amplitude.</Description>
   <Units>radians</Units>
   <Field>
    <Qualifier>PhaseAngle</Qualifier>
    <FieldQuantity>Magnetic</FieldQuantity>
   </Field>
  </Parameter>
  <Parameter>
   <Name>Amplitude Observed from ABOVE Station to NDK</Name>
   <Description>Amplitude of VLF transmitter NDK observed at ABOVE station&quot;
Amplitude is calculated separately for North-South and East-West antennas.</Description>
   <Units>ADC units</Units>
   <Field>
    <Qualifier>Average</Qualifier>
    <FieldQuantity>Magnetic</FieldQuantity>
   </Field>
  </Parameter>
  <Parameter>
   <Name>Bearing Observed From ABOVE Station to NDK</Name>
   <Description>Bearing to VLF transmitter NDK
Bearing direction is based on amplitudes from the 2 antennas and always between 0 (N/S) and 90 (E/W) degrees.
</Description>
   <Units>degrees</Units>
   <Field>
    <Qualifier>DirectionAngle</Qualifier>
    <FieldQuantity>Magnetic</FieldQuantity>
   </Field>
  </Parameter>
  <Parameter>
   <Name>Phase observed from ABOVE station to NAA</Name>
   <Description>Phase of VLF transmitter NAA
Phases for upper (space) and lower (mark) frequencies have been combined. Phase is calculated along direction of maximum amplitude.</Description>
   <Units>radians</Units>
   <Field>
    <Qualifier>PhaseAngle</Qualifier>
    <FieldQuantity>Magnetic</FieldQuantity>
   </Field>
  </Parameter>
  <Parameter>
   <Name>Amplitude Observed from ABOVE Station to NAA</Name>
   <Description>Amplitude of VLF transmitter NAA observed at ABOVE station&quot;
Amplitude is calculated separately for North-South and East-West antennas.</Description>
   <Units>ADC units</Units>
   <Field>
    <Qualifier>Average</Qualifier>
    <FieldQuantity>Magnetic</FieldQuantity>
   </Field>
  </Parameter>
  <Parameter>
   <Name>Bearing Observed From ABOVE Station to NAA</Name>
   <Description>Bearing to VLF transmitter NAA
Bearing direction is based on amplitudes from the 2 antennas and always between 0 (N/S) and 90 (E/W) degrees.
</Description>
   <Units>degrees</Units>
   <Field>
    <Qualifier>DirectionAngle</Qualifier>
    <FieldQuantity>Magnetic</FieldQuantity>
   </Field>
  </Parameter>
  <Parameter>
   <Name>Phase observed from ABOVE station to NLK</Name>
   <Description>Phase of VLF transmitter NLK
Phases for upper (space) and lower (mark) frequencies have been combined. Phase is calculated along direction of maximum amplitude.</Description>
   <Units>radians</Units>
   <Field>
    <Qualifier>PhaseAngle</Qualifier>
    <FieldQuantity>Magnetic</FieldQuantity>
   </Field>
  </Parameter>
  <Parameter>
   <Name>Amplitude Observed from ABOVE Station to NLK</Name>
   <Description>Amplitude of VLF transmitter NLK observed at ABOVE station&quot;
Amplitude is calculated separately for North-South and East-West antennas.</Description>
   <Units>ADC units</Units>
   <Field>
    <Qualifier>Average</Qualifier>
    <FieldQuantity>Magnetic</FieldQuantity>
   </Field>
  </Parameter>
  <Parameter>
   <Name>Bearing Observed From ABOVE Station to NLK</Name>
   <Description>Bearing to VLF transmitter NLK
Bearing direction is based on amplitudes from the 2 antennas and always between 0 (N/S) and 90 (E/W) degrees.
</Description>
   <Units>degrees</Units>
   <Field>
    <Qualifier>DirectionAngle</Qualifier>
    <FieldQuantity>Magnetic</FieldQuantity>
   </Field>
  </Parameter>
  <Parameter>
   <Name>Phase observed from ABOVE station to NWC</Name>
   <Description>Phase of VLF transmitter NWC
Phases for upper (space) and lower (mark) frequencies have been combined. Phase is calculated along direction of maximum amplitude.</Description>
   <Units>radians</Units>
   <Field>
    <Qualifier>PhaseAngle</Qualifier>
    <FieldQuantity>Magnetic</FieldQuantity>
   </Field>
  </Parameter>
  <Parameter>
   <Name>Amplitude Observed from ABOVE Station to NWC</Name>
   <Description>Amplitude of VLF transmitter NWC observed at ABOVE station&quot;
Amplitude is calculated separately for North-South and East-West antennas.</Description>
   <Units>ADC units</Units>
   <Field>
    <Qualifier>Average</Qualifier>
    <FieldQuantity>Magnetic</FieldQuantity>
   </Field>
  </Parameter>
  <Parameter>
   <Name>Bearing Observed From ABOVE Station to NWC</Name>
   <Description>Bearing to VLF transmitter NWC
Bearing direction is based on amplitudes from the 2 antennas and always between 0 (N/S) and 90 (E/W) degrees.
</Description>
   <Units>degrees</Units>
   <Field>
    <Qualifier>DirectionAngle</Qualifier>
    <FieldQuantity>Magnetic</FieldQuantity>
   </Field>
  </Parameter>
  <Parameter>
   <Name>Phase observed from ABOVE station to NPM</Name>
   <Description>Phase of VLF transmitter NPM
Phases for upper (space) and lower (mark) frequencies have been combined. Phase is calculated along direction of maximum amplitude.</Description>
   <Units>radians</Units>
   <Field>
    <Qualifier>PhaseAngle</Qualifier>
    <FieldQuantity>Magnetic</FieldQuantity>
   </Field>
  </Parameter>
  <Parameter>
   <Name>Amplitude Observed from ABOVE Station to NPM</Name>
   <Description>Amplitude of VLF transmitter NPM observed at ABOVE station&quot;
Amplitude is calculated separately for North-South and East-West antennas.</Description>
   <Units>ADC units</Units>
   <Field>
    <Qualifier>Average</Qualifier>
    <FieldQuantity>Magnetic</FieldQuantity>
   </Field>
  </Parameter>
  <Parameter>
   <Name>Bearing Observed From ABOVE Station to NPM</Name>
   <Description>Bearing to VLF transmitter NPM
Bearing direction is based on amplitudes from the 2 antennas and always between 0 (N/S) and 90 (E/W) degrees.
</Description>
   <Units>degrees</Units>
   <Field>
    <Qualifier>DirectionAngle</Qualifier>
    <FieldQuantity>Magnetic</FieldQuantity>
   </Field>
  </Parameter>
  <Parameter>
   <Name>Phase observed from ABOVE station to NRK</Name>
   <Description>Phase of VLF transmitter NRK
Phases for upper (space) and lower (mark) frequencies have been combined. Phase is calculated along direction of maximum amplitude.</Description>
   <Units>radians</Units>
   <Field>
    <Qualifier>PhaseAngle</Qualifier>
    <FieldQuantity>Magnetic</FieldQuantity>
   </Field>
  </Parameter>
  <Parameter>
   <Name>Amplitude Observed from ABOVE Station to NRK</Name>
   <Description>Amplitude of VLF transmitter NRK observed at ABOVE station&quot;
Amplitude is calculated separately for North-South and East-West antennas.</Description>
   <Units>ADC units</Units>
   <Field>
    <Qualifier>Average</Qualifier>
    <FieldQuantity>Magnetic</FieldQuantity>
   </Field>
  </Parameter>
  <Parameter>
   <Name>Bearing Observed From ABOVE Station to NRK</Name>
   <Description>Bearing to VLF transmitter NRK
Bearing direction is based on amplitudes from the 2 antennas and always between 0 (N/S) and 90 (E/W) degrees.
</Description>
   <Units>degrees</Units>
   <Field>
    <Qualifier>DirectionAngle</Qualifier>
    <FieldQuantity>Magnetic</FieldQuantity>
   </Field>
  </Parameter>
  <Parameter>
   <Name>Phase observed from ABOVE station to WWVB</Name>
   <Description>Phase of VLF transmitter WWVB
Phases for upper (space) and lower (mark) frequencies have been combined. Phase is calculated along direction of maximum amplitude.</Description>
   <Units>radians</Units>
   <Field>
    <Qualifier>PhaseAngle</Qualifier>
    <FieldQuantity>Magnetic</FieldQuantity>
   </Field>
  </Parameter>
  <Parameter>
   <Name>Amplitude Observed from ABOVE Station to WWVB</Name>
   <Description>Amplitude of VLF transmitter WWVB observed at ABOVE station&quot;
Amplitude is calculated separately for North-South and East-West antennas.</Description>
   <Units>ADC units</Units>
   <Field>
    <Qualifier>Average</Qualifier>
    <FieldQuantity>Magnetic</FieldQuantity>
   </Field>
  </Parameter>
  <Parameter>
   <Name>Bearing Observed From ABOVE Station to WWVB</Name>
   <Description>Bearing to VLF transmitter WWVB
Bearing direction is based on amplitudes from the 2 antennas and always between 0 (N/S) and 90 (E/W) degrees.
</Description>
   <Units>degrees</Units>
   <Field>
    <Qualifier>DirectionAngle</Qualifier>
    <FieldQuantity>Magnetic</FieldQuantity>
   </Field>
  </Parameter>
 </NumericalData>
</Spase>
